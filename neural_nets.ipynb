{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "0551ee23-2aa2-4d79-8a36-4338cce4213f",
   "metadata": {},
   "source": [
    "# Neural networks and backpropagation\n",
    "\n",
    "In this notebook we will exercise the feedforward, fully-connected neural network, aka multilayer perceptron (MLP). With a single hidden layer it looks like this:\n",
    "\n",
    "![](figures/Colored_neural_network.svg)\n",
    "\n",
    "Every neuron takes a list of inputs $x_i$, applies a weighted sum and feeds the result into an activation function $f$ such that the output $a_i$ is given by\n",
    "\n",
    "$$a_i = f\\left(\\sum_j w_{ij} x_j + b_i\\right) \\quad \\text{or} \\quad \\vec a = f(W\\vec x + \\vec b)$$\n",
    "\n",
    "Note how the argument of the activation function is essentially a **matrix multiplication** of the weight matrix with the input vector! In addition to that we typically add a bias $b$ (like the \"intercept\" in linear models) to each neuron which is also a learnable parameter.\n",
    "\n",
    "For an arbitrary number of layers the output vector of layer $l$ $\\vec a^(l)$ is then calculated from the output vector $\\vec a^{(l-1)}$ of the previous layer:\n",
    "\n",
    "$$\\vec a^{(l)} = f(W^{(l)}\\vec a^{(l-1)} + \\vec b^{(l)})$$\n",
    "\n",
    "Popular choices for the activation function include ([source][2]):\n",
    "\n",
    "![1]\n",
    "\n",
    "We will implement a simple 1-hidden-layer neural network first using available libraries and then manually.\n",
    "\n",
    "[1]: figures/activation_functions.png \"overview of commonly used activation functions\"\n",
    "[2]: https://stanford.edu/~shervine/teaching/cs-229/cheatsheet-deep-learning"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2871842d-1939-44fd-80e3-b198e702d806",
   "metadata": {},
   "source": [
    "We use again our toy example for the 2-moons 2D dataset:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f540d8c5-2c20-49d9-937c-e1214e47d1b5",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from sklearn.datasets import make_moons\n",
    "import matplotlib.pyplot as plt\n",
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "063409f7-c113-4df0-a122-950384169a50",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "X, y = make_moons(noise=0.25, n_samples=100, random_state=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5a37740d-2c06-4413-b4ff-d4db54168210",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.scatter(*X[y==0].T)\n",
    "plt.scatter(*X[y==1].T)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19484b8f-7233-4140-864c-274c185b2b0f",
   "metadata": {},
   "source": [
    "# With sklearn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80a88b74-6f91-4926-a1e7-2f88218a8999",
   "metadata": {},
   "source": [
    "A simple MLP classifier is available in scikit-learn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "af24d579-8f00-40e4-a917-df0131adfdcb",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "from sklearn.neural_network import MLPClassifier"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "01cbdcc1-dd68-41e0-9f6a-95a800e6890b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "model = MLPClassifier().fit(X, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c833d89d-22be-4431-a0b7-22987165308b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(model.loss_curve_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26f499f9-458a-4972-9e35-d2b08de77347",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def visualize_classifier(predict, xmin, xmax, ymin, ymax, **kwargs):\n",
    "    xx, yy = np.meshgrid(\n",
    "        np.linspace(xmin, xmax, 100),\n",
    "        np.linspace(ymin, ymax, 100),\n",
    "    )\n",
    "    X = np.stack([xx, yy], axis=-1).reshape(-1, 2)\n",
    "    zz = predict(X).reshape(xx.shape)\n",
    "    plt.contourf(xx, yy, zz, levels=100, **kwargs)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c2e4691e-6bb6-4670-bb75-c2a16ee21cfd",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "visualize_classifier(lambda x: model.predict_proba(x)[:, 1], -2, 2.5, -2, 2.5, cmap=\"RdBu\")\n",
    "plt.scatter(*X[y==0].T, color=\"red\")\n",
    "plt.scatter(*X[y==1].T, color=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a6fd5cd9-13f7-4684-a2a7-5815f3bdf7df",
   "metadata": {},
   "source": [
    "Let's tune the parameters a bit to make a relatively simple model that we can then reproduce more and more manually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "54b3f5f0-e131-4b8e-b396-0898bdccf1c4",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "model = MLPClassifier(\n",
    "    hidden_layer_sizes=(32,), solver=\"sgd\", batch_size=len(X), learning_rate_init=0.2, max_iter=1000\n",
    ").fit(X, y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8befdc1b-290b-41af-9ac3-c20991f1d102",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(model.loss_curve_)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b2f6a19e-ab7f-4468-a3f5-5652d8bca78c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "visualize_classifier(lambda x: model.predict_proba(x)[:, 1], -2, 2.5, -2, 2.5, cmap=\"RdBu\")\n",
    "plt.scatter(*X[y==0].T, color=\"red\")\n",
    "plt.scatter(*X[y==1].T, color=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9006c5a9-a851-48e9-b12e-33ba8fc557ef",
   "metadata": {},
   "source": [
    "# With pytorch\n",
    "\n",
    "[PyTorch](https://pytorch.org) is one of the most popular machine learning libraries to date (2024). It is mainly developed by Meta AI. Have a look at the [tutorials](https://pytorch.org/tutorials) to learn more.\n",
    "\n",
    "Other popular choices are [TensorFlow](https://www.tensorflow.org)/[Keras](https://keras.io) and [Jax](https://jax.readthedocs.io)\n",
    "\n",
    "The main object in torch are so called *tensors* which have a very similar API to numpy arrays. PyTorch builds computation graphs dynamically which allows for a lot of flexibility and easy debugging."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "588e6a9c-b3ff-47d8-aa34-08c1a401c63f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "import torch\n",
    "from torch import nn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b5a655dc-fcf8-440e-b73b-ea3eee2fb680",
   "metadata": {},
   "source": [
    "To convert a numpy array to a torch tensor, we can just call the `torch.tensor` constructor on it.\n",
    "\n",
    "For some operations PyTorch is rather strict not to mix data types of different precision. Since most NN parameters are initialized as 32 bit floating point numbers we will also convert our data to this type:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a55c2557-bab9-4658-9c0f-c900924a9b6c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "X, y = torch.tensor(X, dtype=torch.float32), torch.tensor(y, dtype=torch.float32)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "de4d28b2-3884-45fd-aaeb-19fb430ca6e8",
   "metadata": {},
   "source": [
    "We create our 1-hidden-layer MLP using the `nn.Sequential` constructor:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22ae710e-ac79-4475-b104-d009ed9cc288",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "neurons = 32\n",
    "model = nn.Sequential(nn.Linear(2, neurons), nn.ReLU(), nn.Linear(neurons, 1), nn.Sigmoid())\n",
    "model"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a4d3cb6d-d97d-47b4-87cb-c71b44045dbc",
   "metadata": {},
   "source": [
    "The `torch.optim` package contains implementations of various optimization algorithms. Here we will just use the plain stochastic gradient descent algorithm:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "02feeabb-124a-4c06-b81f-74e4a066793d",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "optimizer = torch.optim.SGD(model.parameters(), lr=1.0)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8777cb6-8da7-4eb1-ac6d-760ab178b090",
   "metadata": {},
   "source": [
    "We told the optimizer about the parameters of the model (which are also just torch tensors) and when we later call `.step()` the optimizer will apply it's update rule using the gradients that have been attached to the parameters."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "553b4ffb-2c6b-49d5-b569-67f4551c00af",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h2>Exercise 1</h2><br> How many parameters has this model?\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "169449fb-80d7-4c50-a24f-d8e75efa392c",
   "metadata": {},
   "source": [
    "Let's fit the model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "99c22e17-7968-48c4-9eb4-f6bb3dbb7afa",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "history = []\n",
    "for i in range(1000):\n",
    "    optimizer.zero_grad()\n",
    "    y_pred = model(X).squeeze(1)\n",
    "    loss = nn.functional.binary_cross_entropy(y_pred, y)\n",
    "    loss.backward()\n",
    "    history.append(loss.item())\n",
    "    optimizer.step()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f0acdd34-5f57-455c-b466-1b01eb9602b9",
   "metadata": {},
   "source": [
    "* `optimizer.zero_grad()` resets all gradients to 0. By default torch accumulates gradients if a future backpropagation step is executed. **Don't forget this**\n",
    "* the forward pass is calculated by `model(X)` - the `.squeeze(1)` changes the shape of the output from (N, 1) to (N,)\n",
    "* `loss` is our objective we want to minimize, in this case the binary cross entropy (or negative log likelihood)\n",
    "* `loss.backward()` will run the backward pass and attach the gradient of the loss w.r.t `.grad` attribute of all parameters that have `requires_grad=True` set\n",
    "* `optimizer.step()` will perform the actual gradient update"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "39a30eb2-0be9-4931-870a-4c78fa534f05",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(history)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "ec7db594-01fb-4133-b20a-acc653cc1364",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "with torch.no_grad():\n",
    "    visualize_classifier(\n",
    "        lambda x: model(torch.tensor(x, dtype=torch.float32)).squeeze().numpy(),\n",
    "        -2, 2.5, -2, 2.5,\n",
    "        cmap=\"RdBu\"\n",
    "    )\n",
    "plt.scatter(*X[y==0].T, color=\"red\")\n",
    "plt.scatter(*X[y==1].T, color=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8cc7a798-a9b9-4646-a316-d0bcc67d9517",
   "metadata": {},
   "source": [
    "# With torch (manual optimizer)\n",
    "\n",
    "Next, let's leave out the optimizer and perform the gradient update manually, still using pytorch to get the gradient.\n",
    "\n",
    "To get the gradient of a loss w.r.t. some parameters we have to set `requires_grad=True` for the corresponding tensors:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cf518893-3564-42c1-b2bf-8febaed4f80e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "x = torch.linspace(0, 2*np.pi, 100, requires_grad=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "51d9c17e-b652-48ea-8cb5-c4b1e077be52",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "f = torch.sin(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ef3e6a1a-a91a-4fa9-bee3-ff51c78f5749",
   "metadata": {},
   "source": [
    "Now, all tensors that are created as operations of this will have a `grad_fn` attribute:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26b8da61-919c-4a98-84dd-e77562ba9446",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "f.grad_fn"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bcd34080-e66a-463a-9455-ffec05d3e4f7",
   "metadata": {},
   "source": [
    "To convert such tensors (implicitly or explicitly) to numpy arrays we need to \"detach\" them from the computation graph:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7d01e129-f534-44d0-9d0d-bfeec59fdea5",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(x.detach(), f.detach())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "051ae27f-19bf-4b91-9f17-d2be53bb93a6",
   "metadata": {},
   "source": [
    "## The VJP\n",
    "What does `grad_fn` do? It will calculate the so called *vector-Jacobian-product* (VJP)\n",
    "\n",
    "What is that? To calculate the gradient of a loss function we use the chain rule, in the single variable case\n",
    "\n",
    "$$\\frac{\\partial f(g(x))}{\\partial x} = \\frac{\\partial f}{\\partial g}\\frac{\\partial g}{\\partial x}$$\n",
    "\n",
    "and in the [multivariable case](https://en.wikipedia.org/wiki/Chain_rule#General_rule:_Vector-valued_functions_with_multiple_inputs) ($x\\in \\mathbb{R}^n, g: \\mathbb{R}^n\\rightarrow\\mathbb{R}^m, f: \\mathbb{R}^m\\rightarrow\\mathbb{R}^k$)\n",
    "\n",
    "$$\\mathbf{J}_f(g(x)) = \\mathbf{J}_f(g)\\mathbf{J}_g(x),\\quad \\text{in components} \\quad \\frac{\\partial f_i}{\\partial x_j}=\\frac{\\partial f_i}{\\partial g_k}\\frac{\\partial g_k}{\\partial x_j}$$\n",
    "\n",
    "For **the gradient of a scalar** (loss):\n",
    "\n",
    "$$\\frac{\\partial f}{\\partial x_j}=\\underbrace{\\frac{\\partial f}{\\partial g_k}}_{\\mathrm{vector}}\\underbrace{\\frac{\\partial g_k}{\\partial x_j}}_{\\mathrm{Jacobian}}$$\n",
    "\n",
    "So we need to matrix multiply the (**incoming**) gradient (row) **vector** with the **Jacobian** in each step, the vector-Jacobian-product **VJP**.\n",
    "\n",
    "The cool thing: usually we **don't need to compute the full Jacobian to get the VJP!**\n",
    "\n",
    "E.g. here, our tensor `f` applies a sin function componentwise to an input `x`. It's pretty clear we don't need to compute the Jacobian (it's a diagonal matrix).\n",
    "\n",
    "So, the VJP-way of calculating the derivative componentwise would be to take an incoming gradient vector that is all ones:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "53cdf4e6-ebb9-4752-9067-e1d0dcec024f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "v = torch.ones_like(f)\n",
    "v"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "356b3e6a-bdeb-47c9-9b04-c39af86cd6d6",
   "metadata": {},
   "source": [
    "and calculate the VJP by just multiplying this with the componentwise derivative, here the cosine:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8bce9e44-8e25-4455-a45c-ace54d99fbdf",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "vjp = v * torch.cos(x)\n",
    "vjp"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "00772ddf-a6e2-447c-8583-ebb4fd3c4ce7",
   "metadata": {},
   "source": [
    "This is also what `grad_fn` will do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09fafcb2-f6a1-4c5b-a74e-36aebea9008f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "f.grad_fn(v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80ddc666-09a0-46ac-9f26-1508ed458bb3",
   "metadata": {},
   "source": [
    "`.backward` will then go through the whole chain of computations backwards and calculate the VJP in each step.\n",
    "\n",
    "Here we only have one step. Since our tensor is not a scalar we need to feed in the incoming gradient as well:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "140f3e8c-2ae1-4156-a1e3-1275906f1ff7",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "f.backward(v)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "469ff8b9-5042-4b64-be8d-22d32a454ea2",
   "metadata": {},
   "source": [
    "This doesn't return anything, but rather attach the resulting gradient to all tensors with `requires_grad=True` (in torch called \"leaf\" tensors)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "87505015-82ab-4646-a29c-bda8193c2f41",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "x.grad"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1b7545c9-454c-4db9-9120-6dc45ae5bc7c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(x.detach(), f.detach())\n",
    "plt.plot(x.detach(), x.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4876eb59-ef6e-416b-ac30-6a42aa015e2b",
   "metadata": {},
   "source": [
    "With that, we will now fit our neural network again, implementing the gradient step manually:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "23553fac-d239-4349-b7c0-97072e91ed67",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "model = nn.Sequential(nn.Linear(2, neurons), nn.ReLU(), nn.Linear(neurons, 1), nn.Sigmoid())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "50c8eabd-086f-4a68-a82a-e5728f561d00",
   "metadata": {},
   "source": [
    "To update a tensor in-place, adding a value we can use the `add_` method. Our gradient update will just add the negative gradient, scaled by a learning rate `lr` to each of parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3014659c-91a2-469a-b4b7-40064868a574",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "def step(lr=1):\n",
    "    with torch.no_grad():\n",
    "        for par in model.parameters():\n",
    "            par.add_(-lr * par.grad)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9fdcaf52-0e01-4314-b415-afd443175cd0",
   "metadata": {},
   "source": [
    "The `torch.no_grad()` context manager ensures that all operations in this block won't be attached to the computation graph.\n",
    "\n",
    "The training loop becomes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b8811713-24a0-4132-b2f5-89b898930f4a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "history = []\n",
    "for i in range(1000):\n",
    "    model.zero_grad()\n",
    "    y_pred = model(X).squeeze(1)\n",
    "    loss = nn.functional.binary_cross_entropy(y_pred, y)\n",
    "    loss.backward()\n",
    "    step()\n",
    "    history.append(loss.detach().item())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72fa73e1-d7d7-4313-b658-e5a102c7d285",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "plt.plot(history)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b9be18fd-8d3b-4881-9ee5-af5b7c66c2c7",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "with torch.no_grad():\n",
    "    visualize_classifier(lambda x: model(torch.tensor(x, dtype=torch.float32)).squeeze(1), -2, 2.5, -2, 2.5, cmap=\"RdBu\")\n",
    "plt.scatter(*X[y==0].T, color=\"red\")\n",
    "plt.scatter(*X[y==1].T, color=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1a49f88e-4e83-4283-b641-58c5b1f6e28b",
   "metadata": {},
   "source": [
    "# With torch (manual backpropagation)\n",
    "\n",
    "Now we want to also calculate the backward pass completely manually, meaning taking the VJP in each step ourself.\n",
    "\n",
    "This part is inspired by [part 4](https://www.youtube.com/watch?v=q8SA3rM6ckI) of [Andrej Karpathy's *Neural Networks: Zero to Hero* tutorials](https://www.youtube.com/playlist?list=PLAqhIrjkxbuWI23v9cThsA9GvCAUhRvKZ)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f2bfce25-4543-431b-802b-74b5894a14e3",
   "metadata": {},
   "source": [
    "First, when we define the parameters of the network we will also need to decide for starting values.\n",
    "\n",
    "Typically they are initialized to small random values around 0. While there are [schemes that optimize this](https://pytorch.org/docs/stable/nn.init.html#torch.nn.init.kaiming_uniform_), here we will just take a standard normal distribution, scaled by 0.1 for all the parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cd7e44b3-46d3-4e28-b7e9-3e096b043af7",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "w1 = (torch.randn(2, neurons) * 0.1).requires_grad_()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "08f7faa6-1ed3-4f1c-8a49-49c29b3a7f90",
   "metadata": {},
   "source": [
    "Let's start with a very simple neural network with one hidden layer, no bias and use the mean squared error loss function.\n",
    "\n",
    "The first intermediate output is the matrix multiplication of the input vectors with the weight matrix of the hidden layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5e4c3db8-d72b-4c1f-a704-ad327f497ee2",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "z1 = X @ w1; z1.retain_grad()\n",
    "z1.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8bf8511-0bd6-4cf1-a593-4318cb89e39f",
   "metadata": {},
   "source": [
    "This was now a matrix-matrix multiplication since we did this for our whole input data at once! When training neural networks we will almost always work with batches of data, so this is very common."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0f3f5b25-6d26-4e9c-ac88-ae31c5277fed",
   "metadata": {},
   "source": [
    "To crosscheck our manual gradient calculations later we will set `retain_grad` for all intermediate outputs, such that torch will also attach gradients to these in the backward pass.\n",
    "\n",
    "Next, we apply the relu activation function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3e074fcc-4762-4455-b2c0-de4a0b01c60a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "a1 = torch.relu(z1); a1.retain_grad()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "82b9ff0d-8d9f-44c2-b2e6-a94512c0393b",
   "metadata": {},
   "source": [
    "Then we continue to the second (final) layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f109d2ec-c33b-4b14-bcba-d9ef352a5ab9",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "w2 = (torch.randn(neurons, 1) * 0.1).requires_grad_()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e71bbd96-83b1-4036-b189-ba2c6971186e",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "z2 = a1 @ w2; z2.retain_grad()\n",
    "z2.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "19f4cc49-a86a-4e2f-b3e5-2bef08830be3",
   "metadata": {},
   "source": [
    "Since this is the final output with one value per data point we will squeeze the last dimension:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "06804efd-680c-436a-a760-cb1ed525cab5",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "z2 = z2.squeeze(1); z2.retain_grad()\n",
    "z2.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "27151b0f-bebc-48c3-8587-e8182fc06477",
   "metadata": {},
   "source": [
    "We don't apply an activation function, but instead use the linear output to calculate the mean squared error:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "888a4c4e-c333-4107-a980-d3ad6f77a29f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "loss = torch.mean((y - z2) ** 2)\n",
    "loss"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e2ece212-2fce-4827-be4d-e48e4f6cb46e",
   "metadata": {},
   "source": [
    "For crosscheck we run the backward pass with torch:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f80d032b-5e7d-4396-b962-54600734b745",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "loss.backward()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "12ff90ca-c80d-4d68-842c-aa28f2d65ccd",
   "metadata": {},
   "source": [
    "Now we manually go backwards. We will create variables with `d` in front of them where e.g. `dz2` means gradient of loss wrt all components of `z2`\n",
    "\n",
    "The gradient of the loss $L = \\frac{1}{N} \\sum_i (y_i - \\hat{y}_i)^2$ is given by $\\frac{\\partial L}{\\partial \\hat{y}_i} = -\\frac{2}{N}(y_i - \\hat{y}_i)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fc9dce05-fc2f-41d8-a474-385e145b545c",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dz2 = - 2 / len(z2) * (y - z2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0b1d5992-cc24-4527-8219-fafaa05516eb",
   "metadata": {},
   "source": [
    "crosscheck with what torch got:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9bc8d3f6-c425-4ddf-ba28-06f3d6ec4a90",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "(z2.grad == dz2).all()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d692c69b-abc7-4d55-9818-cfc909b25c8c",
   "metadata": {},
   "source": [
    "Now we need the VJP for a matrix multiplication"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c023c190-e36c-4188-8c56-774be954848a",
   "metadata": {},
   "source": [
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h2>Exercise 2</h2>\n",
    "    What is the VJP for a matrix multiplication?<br>\n",
    "    <b>Hint:</b> It's also going to be a matrix multiplication. You can try to guess this from the shapes of the involved tensors.\n",
    "    It's also instructive to derive it once on a sheet of paper.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8e70768-ae7c-4e8e-b64d-82950d00412e",
   "metadata": {},
   "source": [
    "The matrix multiplication was `z2 = a1 @ w2`\n",
    "\n",
    "Since we squeezed the last dimension of `z2` we will have to `unsqueeze` it again for the following operation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "22727f08-5122-41cd-8adb-b2e1e7b1a8e6",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dz2.unsqueeze(1).shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2c2f7547-f6d3-4cb8-983a-c319afc5a802",
   "metadata": {},
   "source": [
    "So we have the following tensor shapes"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6af681a4-817e-47a4-af78-6077c3acc2eb",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dz2.unsqueeze(1).shape, a1.shape, w2.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c9103d4-eebb-491e-9ec5-d318e425bd70",
   "metadata": {},
   "source": [
    "From this we need to get `da1` and `dw2`, the gradients w.r.t `a1` and `w2` via a VJP with the `dz2` gradient vector"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "09c80b46-9bd5-4d61-91ad-b02d6f72c8aa",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dw2 = ... # your task"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fdba22dd-6798-49f5-9f3d-b07a4c9534ad",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dw2.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aefc66d2-4913-47b0-8894-f0f4d8eacff4",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "(dw2 == w2.grad).all()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e7b3538f-15d0-4b43-a50b-0b2b4c5a5da7",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "da1 = ... # your task\n",
    "da1.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bc8f872c-25de-431c-97fc-8e5293459f5b",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "(da1 == a1.grad).all()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddfa7e85-e2e8-46a0-8909-886cad0818e9",
   "metadata": {},
   "source": [
    "What is the derivative of relu?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7f6e16e1-1acb-4f8c-99c9-1d086fbc968a",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dz1 = (z1 > 0) * da1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c7021fc7-505b-4fe4-8b7c-eb703440156f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "(dz1 == z1.grad).all()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6d75903e-0b24-4a02-8d90-63a8fb0c01ae",
   "metadata": {},
   "source": [
    "And another matrix multiplication to get the gradient w.r.t. the weights of the first layer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "485d1f4a-fa4c-474d-8a2c-cf5237c7a7a6",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "w1.shape, dz1.shape, X.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "991ddaa2-a9cb-4371-9d58-4e9d107e6338",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "dw1 = ... # your task\n",
    "dw1.shape"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d7a1e33e-092a-4b13-9043-50ddaffe656f",
   "metadata": {
    "tags": []
   },
   "outputs": [],
   "source": [
    "(dw1 == w1.grad).all()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d8cca1b4-a8ec-4ec1-a3a6-fd6ac5093e09",
   "metadata": {},
   "source": [
    "The full training loop then looks like this:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2abeeb75-764e-460b-a907-683c7fbf1cda",
   "metadata": {},
   "outputs": [],
   "source": [
    "# initialize parameters\n",
    "w1 = torch.randn(2, neurons) * 0.1\n",
    "w2 = torch.randn(neurons, 1) * 0.1\n",
    "\n",
    "# training loop\n",
    "lr = 0.1\n",
    "history = []\n",
    "for i in range(100):\n",
    "    # forward\n",
    "    z1 = X @ w1\n",
    "    a1 = torch.relu(z1)\n",
    "    z2 = a1 @ w2\n",
    "    z2 = z2.squeeze(1)\n",
    "    loss = torch.mean((y - z2) ** 2)\n",
    "    \n",
    "    history.append(loss.item())\n",
    "\n",
    "    # backward\n",
    "    dz2 = - 2 / len(z2) * (y - z2)\n",
    "    dw2 = ...\n",
    "    da1 = ...\n",
    "    dz1 = (z1 > 0) * da1\n",
    "    dw1 = ...\n",
    "\n",
    "    # gradient update\n",
    "    for par, grad in [(w1, dw1), (w2, dw2)]:\n",
    "        par.add_(-lr * grad)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c46ecb54-dd5c-4792-9400-68e311794262",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.plot(history)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "352e31db-c23a-4d5c-9a44-277e5c88c331",
   "metadata": {},
   "outputs": [],
   "source": [
    "with torch.no_grad():\n",
    "    visualize_classifier(\n",
    "        lambda x: torch.relu(torch.tensor(x, dtype=torch.float32) @ w1) @ w2,\n",
    "        -2, 2.5, -2, 2.5, cmap=\"RdBu\"\n",
    "    )\n",
    "plt.scatter(*X[y==0].T, color=\"red\")\n",
    "plt.scatter(*X[y==1].T, color=\"blue\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "28d6d6ef-a0e0-4d8b-a7e7-24fe96b807ab",
   "metadata": {},
   "source": [
    "This does not look too great. There are 3 things we can improve:\n",
    "\n",
    "* implement a bias for the neurons\n",
    "* apply a sigmoid activation function to constrain the output to lie between 0 and 1\n",
    "* switch to the binary cross entropy loss\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h2>Exercise 3</h2>\n",
    "    Add a bias term to the neurons<br>\n",
    "    <b>Hint:</b> The Jacobi matrix of the output of a layer w.r.t. the biases is an identity matrix, so the VJP is the sum over the incoming gradient.<br>\n",
    "    So if we have <code>z = a @ w + b</code>, the gradient <code>db = dz.sum(axis=0)</code>\n",
    "</div>\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h2>Exercise 4</h2>\n",
    "    Apply a sigmoid activation function to the final output.<br>\n",
    "    <b>Hint:</b> You can use <code>z = torch.sigmoid(a)</code>. The derivative of the sigmoid function is given by $f'(x) = f(x)(1-f(x))$\n",
    "</div>\n",
    "\n",
    "<div class=\"alert alert-block alert-success\">\n",
    "    <h2>Exercise 5</h2>\n",
    "    Change the loss function to binary cross entropy.<br>\n",
    "    <b>Reminder:</b> The formula is $L = -\\frac{1}{N}\\sum_i y_i\\log(\\hat y_i) + (1 - y_i)\\log(1 - \\hat y_i)$ for NN outputs $\\hat y_i$ and labels $y_i$\n",
    "</div>"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
